import json
import os

from jinja2 import Template

# get root directory
ROOT_DIR = os.path.dirname(
    os.path.dirname(
        os.path.abspath(__file__)
    )
)

# HTML-template
html_template_dir = os.path.join(ROOT_DIR, 'templates/riddle_answer.html')
with open(html_template_dir, 'r', encoding='utf-8') as f:
    html_template = f.read()

# Load translations
parent_dir_path = os.path.join(os.path.dirname(__file__), os.pardir)
translations_file_path = os.path.join(parent_dir_path, 'translations.json')
with open(translations_file_path, 'r', encoding='utf-8') as f:
    translations = json.load(f)


def localized_text(key):
    return translations[key]


def create_riddle_message(data: dict):
    template = Template(html_template)
    rendered_message = template.render(data)
    return rendered_message

