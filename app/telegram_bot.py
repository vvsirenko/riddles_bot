from telegram import BotCommand, Update, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, \
    KeyboardButton, ReplyKeyboardRemove
from telegram.ext import ContextTypes, Application, ApplicationBuilder, CommandHandler, ConversationHandler, \
    MessageHandler, filters

from app.integrations.integration_client import IntegrationClient
from app.utils import localized_text, create_riddle_message


class ChatTelegramBot:
    """
    Class Telegram Bot
    """
    BEGIN, PROFILE, PUZZLE, RIDDLE, PROVIDED = range(5)

    def __init__(
            self,
            config: dict
    ):
        self.config = config
        self.commands = [
            BotCommand(
                command='start',
                description='Обновить меню'
            ),
            BotCommand(
                command='end',
                description='Закончить диалог'
            ),
            BotCommand(
                command='clear',
                description='Очистить диалог'
            )
        ]
        self.usage = {}
        self.last_message = {}

    async def start(self, update: Update, _: ContextTypes.DEFAULT_TYPE) -> int:
        reply_keyboard = [
            [
                KeyboardButton("📝 Начать диалог"),
                KeyboardButton("🏡 Профиль")
            ]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        greeting_text = f"Привет, {update.message.from_user.name}! \n\nЯ Brain бот!\n"
        description_text = localized_text('start_text')[0]
        start_text = greeting_text + description_text
        await update.message.reply_text(start_text, reply_markup=keyboard)
        return self.BEGIN

    async def begin(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        start_text = "Диалог начат!\n\n" + "Выбери тему"

        reply_keyboard = [
            [
                KeyboardButton("Загадки"),
                KeyboardButton("Пословицы")
            ],
            [KeyboardButton("Закончить диалог")]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )

        await update.message.reply_text(start_text, reply_markup=keyboard)
        return self.PUZZLE

    async def profile(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await update.message.reply_text("Раздел находится в разработке")

    async def riddles(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        reply_keyboard = [
            [
                KeyboardButton("Хочу еще"),
                KeyboardButton("Откадка")
            ],
            [
                KeyboardButton("Вернуться в меню"),
                KeyboardButton("Закончить диалог")
            ]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )

        # answer = await self.get_riddle_text()

        data = {'message': "В гору бегом с горы кувырком"}

        answer = create_riddle_message(data=data)
        await update.message.reply_html(answer, reply_markup=keyboard)
        return self.RIDDLE

    async def proverb(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        reply_keyboard = [
            [
                KeyboardButton("Хочу еще"),
                KeyboardButton("Вернуться в меню")
            ],
            [
                KeyboardButton("Закончить диалог")
            ]
        ]
        keyboard = ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            resize_keyboard=True
        )
        await update.message.reply_text("За двумя зайцами гоняться – ни одного не поймать", reply_markup=keyboard)
        return self.PROVIDED

    async def get_riddle_text(self):
        client = IntegrationClient()
        return await client.get_riddle_text()

    async def post_init(self, application: Application) -> None:
        await application.bot.set_my_commands(self.commands)

    async def end(self, update, _):
        user = update.message.from_user
        # logger.info("Пользователь %s отменил разговор.", user.first_name)
        await update.message.reply_text(
            text='Возвращайтесь к нам скорее',
            reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    def run(self):
        application = ApplicationBuilder() \
            .token(self.config['token']) \
            .post_init(self.post_init) \
            .build()

        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.start)],

            states={
                self.BEGIN: [
                    MessageHandler(
                        filters.Regex("^(📝 Начать диалог)$"), self.begin
                    ),
                    MessageHandler(
                        filters.Regex("^(🏡 Профиль)$"), self.profile
                    )
                ],
                self.PUZZLE: [
                    MessageHandler(
                        filters.Regex("^(Загадки)$"), self.riddles
                    ),
                    MessageHandler(
                        filters.Regex("^(Пословицы)$"), self.proverb
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить диалог)$"), self.end
                    )
                ],
                self.RIDDLE: [
                    MessageHandler(
                        filters.Regex("^(Хочу еще)$"), self.riddles
                    ),
                    MessageHandler(
                        filters.Regex("^(Откадка)$"), self.end
                    ),
                    MessageHandler(
                        filters.Regex("^(Вернуться в меню)$"), self.begin
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить диалог)$"), self.end
                    )
                ],
                self.PROVIDED: [
                    MessageHandler(
                        filters.Regex("^(Хочу еще)$"), self.proverb
                    ),
                    MessageHandler(
                        filters.Regex("^(Вернуться в меню)$"), self.begin
                    ),
                    MessageHandler(
                        filters.Regex("^(Закончить диалог)$"), self.end
                    )
                ]
            },
            fallbacks=[CommandHandler('cancel', self.end)],
        )

        application.add_handler(conv_handler)
        application.run_polling()

